#!/usr/bin/env python

ex=\
"""
Given probe sequences, target sequences, and blat alignments of target sequences, write link.psl file that can be opened in IGB.
"""

import sys,os,argparse,gzip
from Bio import SeqIO

def main(fastafile='data/HG-U133_Plus_2.target',
         probefile='data/HG-U133_Plus_2.probe_tab.gz',
         blatfile='data/GPL570.HG-U133_Plus_2.psl.gz',
         qcov=None):
    records=get_seq_dict(fastafile)
    probes=get_probes_dict(probefile)
    add_locs(records,probes)
    write_psl(records,probes,blatfile,filt=qcov)

# only report alignments where target has filt or better coverage in the alignment to genome
def write_psl(records,probes,blatfile='data/GPL570.HG-U133_Plus_2.psl.gz',
              outfile=None,filt=0):
    fh=read_file(blatfile)
    if outfile:
        outfh=open(outfile,'w')
    else:
        outfh=sys.stdout
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.rstrip().split('\t')
        qsize=int(toks[10])
        matches=float(toks[0])
        qcov=matches/qsize
        if not qcov>=filt:
            continue
        qname=get_probeset(qname=toks[9])
        toks[9]=qname
        rec=records[qname]
        probe_loc=probes[qname]
        txt=make_probe_psl(qname,rec,probe_loc)
        txt='\t'.join(toks)+'\t'+txt
        outfh.write(txt+'\n')
    if outfile:
        outfh.close()


# return an open file handle to compressed or
# uncompressed file
def read_file(fname):
    if fname.endswith('.gz'):
        fh = gzip.GzipFile(fname)
    else:
        fh=open(fname,'rU')
    return fh

# replace None with index of probe into target sequence
# Note: Only returns one probe location; a probe could appear
# in more than one place in the target.
def add_locs(records,probes):
    for probeset_id in records.keys():
        probeslist=probes[probeset_id].keys()
        record=records[probeset_id]
        for probe in probeslist:
            index=record.seq.lower().find(probe.lower())
            if index==-1:
                # problem with AFFX-HUMISGF3A/M97935_5_at
                sys.stderr.write("Warning: Can't find probe %s for %s. Skipping it.\n"%(probe,probeset_id))
            probes[probeset_id][probe]=index
    return probes

def make_probe_psl(qname,rec,probes_dict):
    toks=make_probe_psl_fields(qname,rec,probes_dict)
    return '\t'.join(toks)

wholeline='1290	0	0	0	0	0	5	581	+	ATH1-121501:261585_AT	1290	0	1290	Chr1	30427671	3759	5630	154,281,120,390,153,192,	0,154,435,555,945,1098,	3759,3995,4485,4705,5173,5438,	237	1053	0	0	0	0	0	0	+	P.ATH1-121501:261585_AT	1290	710	1275	ATH1-121501:261585_AT	1290	710	1275	11	25,25,25,25,25,25,25,25,25,25,25,	710,727,744,759,860,876,954,987,1142,1158,1250,	710,727,744,759,860,876,954,987,1142,1158,1250,'

# example line
# files made by Affymetrix contain consecutive numbers, PSL spec says it's the number of matches
# we'll use 0
sample='1053	0	0	0	0	0	0	+	P.ATH1-121501:261585_AT	1290	710	1275	ATH1-121501:261585_AT	1290	710	1275	11	25,25,25,25,25,25,25,25,25,25,25,	710,727,744,759,860,876,954,987,1142,1158,1250,	710,727,744,759,860,876,954,987,1142,1158,1250,'
# see: http://useast.ensembl.org/info/website/upload/psl.html
def make_probe_psl_fields(qname,rec,probes_dict):
    tlen=len(rec)
    positions=filter(lambda x:x!=None,probes_dict.values())
    positions.sort()
    plen=len(probes_dict.keys()[0])
    qlen=sum(map(lambda x:len(x),probes_dict.keys()))
    tname=qname
    qname="P."+qname
    # matches=positions[-1]+plen-positions[0]
    start=positions[0]
    end=positions[-1]+plen
    toks=['0','0','0','0','0','0','0','0','+',
          qname,str(tlen),str(start),str(end),
          tname,str(tlen),str(start),str(end),
          str(len(positions))]
    blockSizes=''
    blockStarts=''
    for position in positions:
        blockSizes=blockSizes+str(plen)+','
        blockStarts=blockStarts+str(position)+','
    toks.append(blockSizes)
    toks.append(blockStarts)
    toks.append(blockStarts)
    return toks



# read probes into memory
# return dictionary where probe set ids are keys
# value is another dictionary with probe sequences
# as keys
def get_probes_dict(probefile='data/HG-U133_Plus_2.probe_tab.gz'):
    d = {}
    if probefile.endswith('.gz'):
        fh = gzip.GzipFile(probefile)
    else:
        fh=open(probefile,'rU')
    fh.readline() # get rid of header
    while 1:
        line=fh.readline()
        if not line:
            fh.close()
            break
        line=line.rstrip()
        toks=line.split('\t')
        probeseq=toks[4]
        probeset_id=toks[0]
        if not d.has_key(probeset_id):
            d[probeset_id]={}
        if d[probeset_id].has_key(probeseq):
            raise ValueError("Duplicate probe in %s"%probeset_id)
        else:
            d[probeset_id][probeseq]=None
    return d

# read target sequences into memory
# use probe set id as name of sequence
# return dictionary where seq id is key
# SeqRecord object is value
def get_seq_dict(fastafile='data/HG-U133_Plus_2.target.gz'):
    if fastafile.endswith('.gz'):
        fh = gzip.GzipFile(fastafile)
    else:
        fh = open(fastafile,'rU')
    records=SeqIO.to_dict(SeqIO.parse(fh,'fasta'),key_function=key_function)
    return records

def get_probeset(qname=None):
    toks=qname.split(':')
    tok=toks[-1]
    if not tok.endswith(';'):
        raise ValueError("Don't recognize id: %s"%tok)
    return tok[0:-1]

# function to get probeset id from target fasta file display names
def key_function(seq):
    return get_probeset(seq.id)

if __name__=='__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('--probes','-p',dest='probefile',
                        help='probe tab file from Affymetrix [REQUIRED]',
                        required=True)
    parser.add_argument('--fasta','-f',dest='fastafile',
                        help='contains target sequences [REQUIRED]',
                        required=True)
    parser.add_argument('--blat','-b',dest='blatfile',
                         help='output from running blat [REQUIRED]')
    parser.add_argument('--query_coverage','-q',dest='qcov',
                        help='minimum query coverage [default is .95]',
                        default=0.95,type=float)
    args=parser.parse_args()
    main(fastafile=args.fastafile,
         probefile=args.probefile,
         blatfile=args.blatfile,
         qcov=args.qcov)



# Ann's notes:
# HG-U133_Plus_2.target has 54675 sequences
# fasta header ids look like: 'target:HG-U133_Plus_2:222675_s_at;'
# http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL570

# first probe start: 4866900
# last probe end: 4867741

# example




# http://www.ensembl.org/info/website/upload/psl.html

# >>> f.find_locs(seqs,probes)
# Can't find probe CAGTTTTCCCATGGAAATCAGACAG for AFFX-HUMISGF3A/M97935_5_at
# Can't find probe GGAGCAGGTTCACCAGCTTTATGAT for AFFX-HUMISGF3A/M97935_5_at

# sort, block compress, then index
# sort -k14,14 -k16,16n FILE.link.psl | bgzip -c > FILE.link.psl.gz
# tabix -s 14 -b 16 -e 17 FILE.link.psl.gz
